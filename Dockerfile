FROM python:3.9-alpine3.14

WORKDIR /api

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY ./app .

EXPOSE 8000

CMD [ "python", "/api/main.py" ]
